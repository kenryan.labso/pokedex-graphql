# Pokedex with GraphQL endpoint

This project was built with React with an open-source GraphQL endpoint.

# To Test

- Clone this repository.

- Extract `pokedex-graphql.zip` file.

- Install npm packages by running `npm install` command.

- Run the project via `npm start`.

- Login using the `static credential` below:
```
  Email: admin@admin.com
  Password: admin
```